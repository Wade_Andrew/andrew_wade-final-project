import express from "express";
import argon2 from "argon2";
import jwt from "express-jwt";
import jsonwebtoken from "jsonwebtoken";
import entryRoutes from "./src/entries";
import userRoutes from "./src/users";
import cors from "cors";

const users = [
  {
    name: "Bob Jones",
    email: "testuser@test.com",
    password: "$argon2i$v=19$m=12,t=3,p=1$MjBhaXZlbnFtYm1pMDAwMA$MRMaiRRr1kxbkTFQdQrV0A",
  }, //Password above is a hashed version of "password"
  {
    name: "Lisa Larson",
    email: "testuser1@test.com",
    password: "$argon2d$v=19$m=12,t=3,p=1$cTl1NGRibnN2cjkwMDAwMA$8H03aC5fbc2VJ0fkKO/5Ww",
    //Password above is a hashed version of "password1"
  },
];

const app = express();
const PORT = process.env.PORT || 4000;

app.use(express.json());
app.use(cors());

app.post("/auth", async (req, res) => {
  const requestUser = req.body.email;
  const password = req.body.password;
  const userFound = users.find(({ email }) => email === requestUser);

  if (userFound && (await argon2.verify(userFound.password, password))) {
    //Create token if username & password match user object
    const token = jsonwebtoken.sign({ requestUser }, process.env.JWT_SECRET, { expiresIn: "60m" });
    return res.json({ token });
  }
  return res.status(401).json({ message: "incorrect credentials provided" });
});

app.use("/contact_form/entries", entryRoutes);
app.use("/users", userRoutes);

app.use(jwt({ secret: process.env.JWT_SECRET, algorithms: ["HS256"] }));

app.listen(PORT, () => {
  console.log(`Server started at http://localhost:${PORT}`);
});
