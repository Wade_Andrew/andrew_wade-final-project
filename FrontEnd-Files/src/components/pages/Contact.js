import React, { useState } from "react";
import "./Contact.css";
import { Form, Input, Label, Container } from "reactstrap";
import Swal from "sweetalert2";
import SideNav from "../shared/SideNav";
import bioPic from "../../images/IMG_2464.JPG";

const Contact = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [content, setContent] = useState("");

  const formSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch("http://localhost:4000/contact_form/entries", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ name, email, phoneNumber, content }),
    });
    const payload = await response.json();
    if (response.status >= 400) {
      Swal.fire({
        text: `Oops! Error: ${payload.message} for fields: ${payload.invalid.join(",")}`,
        icon: "error",
        width: "30rem",
        padding: "0rem 0.1rem 1rem 0.1rem",
        confirmButtonColor: "#1b9cd8",
      });
    } else {
      Swal.fire({
        text: `Thanks for the message, ${name}. I'll get back to you within 24 hours.`,
        icon: "success",
        iconColor: "#00be03",
        width: "30rem",
        padding: "0rem 0.1rem 1rem 0.1rem",
        confirmButtonText: "Cool",
        confirmButtonColor: "#1a9c07",
      });
    }
    setName("");
    setEmail("");
    setPhoneNumber("");
    setContent("");
  };

  let time = new Date().getHours();
  let greeting = "";

  if (time < 12) {
    greeting = "Morning";
  } else if (time > 12 && time < 18) {
    greeting = "Afternoon";
  } else {
    greeting = "Evening";
  }

  return (
    <Container fluid className="p-0">
      <aside className="contact-animation">Get In Touch</aside>
      <main className="contact-box-flex">
        <section className="contact-flex-item-1">
          <hr />
          <p className="greeting">Good {greeting}!</p>
          <p>Please fill out the form to get in touch with me.</p>
          <p>
            I'm happy to answer any questions about my work or anything else you might have in mind.
          </p>
          <hr />
          <img className="bio-photo" src={bioPic} alt="Wade Andrew" />
          <p className="bio-description">This is me in India during the Holi festival</p>
        </section>
        <section className="contact-flex-item-2">
          <Form onSubmit={formSubmit} className="contact-form">
            <div>
              <div>
                <Label for="nameEntry">Name</Label>
              </div>
              <div>
                <Input
                  type="text"
                  placeholder="What's your name?"
                  name="name"
                  id="nameEntry"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  required
                />
              </div>
              <div>
                <Label for="emailEntry">Email Address</Label>
                <Input
                  type="email"
                  placeholder="Enter valid email"
                  name="email"
                  id="emailEntry"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  required
                />
                <Label for="phoneEntry">Phone Number</Label>
                <Input
                  type="tel"
                  pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
                  placeholder="xxx-xxx-xxxx"
                  name="phone"
                  id="phoneEntry"
                  value={phoneNumber}
                  onChange={(e) => setPhoneNumber(e.target.value)}
                  required
                />
                <Label for="messageEntry">Message</Label>
                <Input
                  type="textarea"
                  placeholder="How can I help you?"
                  name="text"
                  id="messageEntry"
                  value={content}
                  onChange={(e) => setContent(e.target.value)}
                  required
                />
              </div>
              <input type="submit" value="Send Message" />
              <hr />
            </div>
          </Form>
          <p className="contact-para">
            Email:&nbsp;{" "}
            <a href="mailto:wadeandrew@icloud.com" className="underline">
              wadeandrew@icloud.com
            </a>
            <br />
            <br />
            Phone:&nbsp; 778.995.5743
          </p>
        </section>
      </main>
      <aside>
        <SideNav />
      </aside>
    </Container>
  );
};

export default Contact;
