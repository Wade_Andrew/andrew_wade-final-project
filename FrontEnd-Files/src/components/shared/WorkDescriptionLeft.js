import React from "react";
import "../pages/Work.css";
import { Container } from "reactstrap";

const WorkDescriptionLeft = (props) => {
  return (
    <Container fluid className="p-0">
      <div className="work-box-flex">
        <div className="work-flex-item-description">
          <p className="description-left">
            {props.descriptionTop}
            <br />
            <br />
            {props.descriptionBottom}
          </p>
          <p className="description-site-para">
            <a href={props.link} target="_blank" rel="noopener noreferrer">
              <span className="small-x-green">x</span>
              <span className="site-link">View Site</span>
            </a>{" "}
            - opens in a new tab
          </p>
        </div>
        <div className="work-flex-item-image">
          <img src={props.image} alt={props.alt} />
        </div>
      </div>
    </Container>
  );
};

export default WorkDescriptionLeft;
