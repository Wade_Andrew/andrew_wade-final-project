import React from "react";
import "../pages/About.css";
import { useInView } from "react-intersection-observer";

const EmploymentList = (props) => {
  const { ref, inView } = useInView({
    triggerOnce: true,
    threshold: 0.5,
  });

  return (
    <section>
      <div>
        <span ref={ref} className={`date ${inView ? "date-animation" : ""}`}>
          {/* "date" is the default className. "date-animation" className is added when the span scrolls into the viewport, which starts the animation */}
          {props.date}
        </span>
        <span className="location">
          <span className="green">{props.position}</span> - {props.workplace}
        </span>
      </div>
      <div>
        <ul>
          {props.experience1 !== "" && <li>{props.experience1}</li>}
          {props.experience2 !== "" && <li>{props.experience2}</li>}
          {props.experience3 !== "" && <li>{props.experience3}</li>}
        </ul>
        <br />
        <hr />
      </div>
    </section>
  );
};

export default EmploymentList;
