const employmentData = [
  {
    id: 1,
    date: "2006 - Present",
    position: "Digital Print Specialist",
    workplace: "Hemlock Printers - Vancouver, BC",
    experience1:
      "I'm the lead press and prepress technician in the Digital department at Hemlock Printers, one of the largest and most well-respected printers in North America.",
    experience2:
      "Hemlock is at the cutting-edge of digital printing, serving customers such as Facebook, Airbnb and the University of British Columbia.",
    experience3:
      "My technical expertise has been instrumental to the growth and success of Hemlock's digital department.",
  },
  {
    id: 2,
    date: "2006 - Present",
    position: "Freelance Photographer",
    workplace: "Vancouver, BC",
    experience1:
      "I have been a freelance photographer in Vancouver for many years. My favourite subjects are portraits, travel and outdoor photography.",
    experience2: "",
    experience3: "",
  },
  {
    id: 3,
    date: "2003 - 2006",
    position: "Digital Print Specialist",
    workplace: "GB Integrated Media - Winnipeg, MB",
    experience1:
      "I was introduced to the exciting world of digital print and design at GB Media. I worked in the press department, running traditional offset as well as digital print equipment.",
    experience2: "",
    experience3: "",
  },
  {
    id: 4,
    date: "2000 - 2003",
    position: "Prepress Technician",
    workplace: "Embassy Graphics - Winnipeg, MB",
    experience1:
      "Embassy Graphics is a custom prepress house that serves book and magazine publishers worldwide.",
    experience2:
      "I was a member of the prepress team at Embassy, where I performed custom retouching and colour correcting of images using Adobe Photoshop.",
    experience3: "",
  },
];

export default employmentData;
