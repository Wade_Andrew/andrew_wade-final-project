const portfolioData = [
  {
    id: 1,
    image: "./images/website_1.JPG",
    alt: "A woman hiking in the mountains",
    descriptionTop:
      "I took this photo of my partner Naomi hiking in British Columbia in 2019. Living in BC, my summers are dedicated to hiking, camping and outdoor photography.",
    descriptionBottom:
      "I created this web page for a fictional British Columbia tourism association.",
    link: "#",
  },
  {
    id: 2,
    image: "./images/website_2.JPG",
    alt: "A man standing in the water holding a shovel",
    descriptionTop:
      "This is the home page for my personal photography website. I have been an enthusiastic photographer for many years, documenting my hiking trips and travel overseas.",
    descriptionBottom: "Big, eye-catching images feature prominently on this website.",
    link: "#",
  },
  {
    id: 3,
    image: "./images/website_3.JPG",
    alt: "A couple beside a lake, a boat in the water, the Taj Mahal",
    descriptionTop:
      "I created this web page for a student project for the York University Web Development program. The assignment was to create an interactive photo gallery.",
    descriptionBottom: "It was my first ever use of Javascript, which I found very exciting!",
    link: "#",
  },
];

export default portfolioData;
